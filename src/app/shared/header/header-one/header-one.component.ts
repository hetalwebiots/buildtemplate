import { Component,  HostListener, Inject, OnInit } from '@angular/core';
import { WINDOW } from '../../services/windows.service';
declare var $: any;
@Component({
  selector: 'app-header-one',
  templateUrl: './header-one.component.html',
  styleUrls: ['./header-one.component.scss']
})
export class HeaderOneComponent implements OnInit {
  public fixedtop : boolean = false;
  constructor(  @Inject(WINDOW) private window) { }

  ngOnInit() {
  }
  
  // @HostListener Decorator    
  @HostListener("window:scroll", ['$event'])
  onWindowScroll(event) {
    let number = this.window.pageYOffset;
    if (number >= 200) { 
      this.fixedtop = true;
    } else {
      this.fixedtop = false;
    }
  }

}
