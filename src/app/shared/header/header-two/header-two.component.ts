import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { WINDOW } from '../../services/windows.service';
@Component({
  selector: 'app-header-two',
  templateUrl: './header-two.component.html',
  styleUrls: ['./header-two.component.scss']
})
export class HeaderTwoComponent implements OnInit {
  public fixedtop : boolean = false;
  constructor( @Inject(WINDOW) private window) { }

  ngOnInit() {
  }

  // @HostListener Decorator    
  @HostListener("window:scroll", ['$event'])
  onWindowScroll(event) {
    let number = this.window.pageYOffset;
    if (number >= 200) { 
      this.fixedtop = true;
    } else {
      this.fixedtop = false;
    }
  }
}
