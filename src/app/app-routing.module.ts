import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoComponent } from './demo/demo.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  { 
    path: '', 
    redirectTo: 'demo', 
    pathMatch: 'full' 
  },
  { 
    path: 'demo',
    component: DemoComponent
  },
  { 
    path : '',
    component : MainComponent,
    children: [ 
      {
        path : 'home',
        loadChildren: './home/home.module#HomeModule'
      }
    ]
      
  },
  { 
    path: '**', 
    redirectTo: 'home/one'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
