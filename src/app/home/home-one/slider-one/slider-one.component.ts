import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider-one',
  templateUrl: './slider-one.component.html',
  styleUrls: ['./slider-one.component.scss']
})
export class SliderOneComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  mySlideOptions={
    autoplay: false,
    center: true,
    loop: true,
    dots: false,
    nav: true,
    navText: ["<span class='icon icon-arrow-left7'></span>","<span class='icon icon-arrow-right7'></span>"],
    items: 1
  };
}
