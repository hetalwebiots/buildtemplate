import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-team-one',
  templateUrl: './team-one.component.html',
  styleUrls: ['./team-one.component.scss']
})
export class TeamOneComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public data = [{
    img: 'assets/images/team/1.jpg',
    name:'John Deo',
    desig:'Founder, CEO',
  }, 
  {
    img: 'assets/images/team/2.jpg',
    name:'John Deo',
    desig:'Founder, CEO',
  },
  {
  img: 'assets/images/team/3.jpg',
  name:'John Deo',
  desig:'Founder, CEO'
  }
  ]

  mySlideOptions={
    autoplay: true,
    loop: true,
    dots: false,
    margin: 30,
    items: 3,
    responsive:{
        0:{
            items:1
        },
        420:{
            items:1
        },
        576:{
            items:2
        },
        768:{
            items:2
        },
        992:{
            items:3
        },
        1000:{
            items:3
        }
    }
  };

}
