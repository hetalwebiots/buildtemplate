import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwlModule } from 'ngx-owl-carousel';

//First Demo Component
import { HomeOneComponent } from './home-one/home-one.component';
import { SliderOneComponent } from './home-one/slider-one/slider-one.component';
import { AboutOneComponent } from './home-one/about-one/about-one.component';
import { ServiceOneComponent } from './home-one/service-one/service-one.component';
import { BannerOneComponent } from './home-one/banner-one/banner-one.component';
import { ProjectOneComponent } from './home-one/project-one/project-one.component';
import { RatingOneComponent } from './home-one/rating-one/rating-one.component';
import { TeamOneComponent } from './home-one/team-one/team-one.component';
import { FaqOneComponent } from './home-one/faq-one/faq-one.component';
import { NewsOneComponent } from './home-one/news-one/news-one.component';
import { TestimonialOneComponent } from './home-one/testimonial-one/testimonial-one.component';
import { BrandOneComponent } from './home-one/brand-one/brand-one.component';

//Second Demo Component
import { HomeTwoComponent } from './home-two/home-two.component';
import { SliderTwoComponent } from './home-two/slider-two/slider-two.component';
import { AboutTwoComponent } from './home-two/about-two/about-two.component';
import { ServiceTwoComponent } from './home-two/service-two/service-two.component';
import { BannerTwoComponent } from './home-two/banner-two/banner-two.component';
import { ProjectTwoComponent } from './home-two/project-two/project-two.component';
import { RatingTwoComponent } from './home-two/rating-two/rating-two.component';
import { TeamTwoComponent } from './home-two/team-two/team-two.component';
import { FaqTwoComponent } from './home-two/faq-two/faq-two.component';
import { NewsTwoComponent } from './home-two/news-two/news-two.component';
import { TestimonialTwoComponent } from './home-two/testimonial-two/testimonial-two.component';
import { BrandTwoComponent } from './home-two/brand-two/brand-two.component';


//Six Demo's Component
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HomeSixComponent } from './home-six/home-six.component';
import { SliderSixComponent } from './home-six/slider-six/slider-six.component';
import { ServiceSixComponent } from './home-six/service-six/service-six.component';
import { AboutSixComponent } from './home-six/about-six/about-six.component';
import { ProjectSixComponent } from './home-six/project-six/project-six.component';
import { BannerSixComponent } from './home-six/banner-six/banner-six.component';
import { TeamSixComponent } from './home-six/team-six/team-six.component';
import { RatingSixComponent } from './home-six/rating-six/rating-six.component';
import { NewsSixComponent } from './home-six/news-six/news-six.component';
import { TestimonialSixComponent } from './home-six/testimonial-six/testimonial-six.component';
import { FaqSixComponent } from './home-six/faq-six/faq-six.component';
import { BrandSixComponent } from './home-six/brand-six/brand-six.component';

//Third Demo component
import { HomeThreeComponent } from './home-three/home-three.component';

@NgModule({
  declarations: [HomeSixComponent, SliderSixComponent, ServiceSixComponent, AboutSixComponent, ProjectSixComponent, BannerSixComponent, TeamSixComponent, RatingSixComponent, NewsSixComponent, TestimonialSixComponent, FaqSixComponent, BrandSixComponent, HomeTwoComponent, SliderTwoComponent, AboutTwoComponent, ServiceTwoComponent, BannerTwoComponent, ProjectTwoComponent, RatingTwoComponent, TeamTwoComponent, FaqTwoComponent, NewsTwoComponent, TestimonialTwoComponent, BrandTwoComponent, HomeOneComponent, SliderOneComponent, AboutOneComponent, ServiceOneComponent, BannerOneComponent, ProjectOneComponent, RatingOneComponent, TeamOneComponent, FaqOneComponent, NewsOneComponent, TestimonialOneComponent, BrandOneComponent, HomeThreeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    OwlModule,
    SharedModule
  ]
})
export class HomeModule { }
