import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-six',
  templateUrl: './news-six.component.html',
  styleUrls: ['./news-six.component.scss']
})
export class NewsSixComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public datas = [{
    img: 'assets/images/blog/1.jpg',
    day:'25',
    month:'April',
    heading:'Sandford stadium improvements',
    description:'Lorem Ipsum is simply dummy text of the print and type setting industry. Lorem Ipsum has'
  }, 
  {
    img: 'assets/images/blog/2.jpg',
    day:'25',
    month:'April',
    heading:'Sandford stadium improvements',
    description:'Lorem Ipsum is simply dummy text of the print and type setting industry. Lorem Ipsum has'  
  },
  {
    img: 'assets/images/blog/3.jpg',
    day:'25',
    month:'April',
    heading:'Sandford stadium improvements',
    description:'Lorem Ipsum is simply dummy text of the print and type setting industry. Lorem Ipsum has'
  }]

  myslideroption=
  {
    autoplay: true,
        loop: true,
       dots: false,
        margin: 30,
        items: 3,
        responsive:{
            0:{
                items:1
            },
            420:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:2
            },
            992:{
                items:3
            },
            1000:{
                items:3
            }
        }
  };
}
