/*-----------------------------------------------------------------------------------

 Template Name: Build
 Template URI: themes.pixelstrap.com/build
 Description: This is Construct website
 Author: Pixelstrap
 Author URI:

 ----------------------------------------------------------------------------------- */
/* 1.Pre Loader *\
 /* 2.Tap on Top *\
 /* 3.Nav Bar Fixed *\
 /* 4.Banner Slider *\
 /* 5.Our Project *\
 /* 6.Comment Section *\
 /* 7.Testimonial Section *\
 /* 8.Testimonial Section_4 *\
 /* 9.Team Section *\
 /* 10.News Section *\
 /* 11.Brand Section *\
 /* 12.Service slider *\
 /* 13.Project Slider *\
 /* 14.Portfolio Slider *\
 /* 15.Rating Start *\
 /* 16.Footer Title Start *\
 /* 17.Search Start *\
 /* 18.Footer Fix */
/*  19.LTR to RTL JS */
/*  20.Color change */


(function($) {
    "use strict";
    /*=====================
     1. Pre Loader
     ==========================*/
    $('.loader-wrapper').fadeOut('slow', function() {
        $(this).remove();
    });


    /*=====================
     2. Tap to Top
     ==========================*/
    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 500) {
            $('.tap-top').fadeIn();
        } else {
            $('.tap-top').fadeOut();
        }
    });
    $('.tap-top').on('click', function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });


    /*=====================
     3. Nav Bar Fixed
     ==========================*/
    // $(window).scroll(function(){
    //     if ($(window).scrollTop() >= 200) {
    //         $('.navbar').addClass('fixed-top');
    //     }
    //     else {
    //         $('.navbar').removeClass('fixed-top');
    //     }
    // });


    /*=====================
     4. Banner Slider
     ==========================*/
        var owl = $('.slide_1');
        // owl.owlCarousel({
        //     autoplay: false,
        //     center: true,
        //     loop: true,
        //     dots: false,
        //     nav: true,
        //     navText: ["<span class='icon icon-arrow-left7'></span>","<span class='icon icon-arrow-right7'></span>"],
        //     items: 1
        // });
        function setAnimation ( _elem, _InOut ) {
            var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            _elem.each ( function () {
                var $elem = $(this);
                var $animationType = 'animated ' + $elem.data( 'animation-' + _InOut );

                $elem.addClass($animationType).one(animationEndEvent, function () {
                    $elem.removeClass($animationType);
                });
            });
        }
        owl.on('change.owl.carousel', function(event) {
            var $currentItem = $('.owl-item', owl).eq(event.item.index);
            var $elemsToanim = $currentItem.find("[data-animation-out]");
            setAnimation ($elemsToanim, 'out');
        });
        var round = 0;
        owl.on('changed.owl.carousel', function(event) {
            var $currentItem = $('.owl-item', owl).eq(event.item.index);
            var $elemsToanim = $currentItem.find("[data-animation-in]");
            setAnimation ($elemsToanim, 'in');
        });
        owl.on('translated.owl.carousel', function(event) {
            console.log (event.item.index, event.page.count);
            if (event.item.index == (event.page.count - 1))  {
                if (round < 1) {
                    round++;
                    console.log (round);
                } else {
                    owl.trigger('stop.owl.autoplay');
                    var owlData = owl.data('owl.carousel');
                    owlData.settings.autoplay = false;
                    owlData.options.autoplay = false;
                    owl.trigger('refresh.owl.carousel');
                }
            }
        });


    /*=====================
     5. Our Project
     ==========================*/
        $(".filter-button").on('click', function() {
            $(this).addClass('active').siblings('.active').removeClass('active');
            var value = $(this).attr('data-filter');
            if(value == "all")
            {
                $('.filter').show('1000');
            }
            else
            {
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');
            }
        });
        $("#formButton").on('click', function() {
            $("#form1").toggle();
        });
        $(".content").slice(0, 6).show();
        $("#loadMore").on("click", function(e){
            e.preventDefault();
            $(".content:hidden").slice(0, 3).slideDown();
            if($(".content:hidden").length == 0) {
                $("#loadMore").text("No Content").addClass("noContent");
            }
        });
        $('.zoom-gallery').magnificPopup({
            delegate: '.zoom_gallery',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                verticalFit: true,
                titleSrc: function(item) {
                    return item.el.attr('title') + ' &middot;';
                }
            },
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300,
                opener: function(element) {
                    return element.find('img');
                }
            }

        });


    /*=====================
     6. Comment Section
     ==========================*/
    $( '.js-input' ).keyup(function() {
        if( $(this).val() ) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });


    /*=====================
     7. Testimonial Section
     ==========================*/
    var testimonial_1 = $(".testimonial_slide");
    testimonial_1.owlCarousel({
        autoplay: true,
        center: true,
        loop: true,
        dots: false,
        items: 1
    });


    /*=====================
     8. Testimonial Section_4
     ==========================*/
    var testimonial_4 = $(".testimonial_slide_4");
    testimonial_4.owlCarousel({
        autoplay: true,
        center: false,
        loop: true,
        dots: false,
        items: 2,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            }
        }
    });


    /*=====================
     9. Team Section
     ==========================*/
    var team_sec = $(".team_slide");
    team_sec.owlCarousel({
        autoplay: true,
        loop: true,
        dots: false,
        margin: 30,
        items: 3,
        responsive:{
            0:{
                items:1
            },
            420:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:2
            },
            992:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });


    /*=====================
     10. News Section
     ==========================*/
    var news_slide = $(".news_slide");
    news_slide.owlCarousel({
        autoplay: true,
        loop: true,
        dots: false,
        margin: 30,
        items: 3,
        responsive:{
            0:{
                items:1
            },
            420:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:2
            },
            992:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });


    /*=====================
     11. Brand Section
     ==========================*/
    var brand_sec = $(".brand_slide");
    brand_sec.owlCarousel({
        autoplay: true,
        loop: true,
        dots: false,
        margin: 30,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        items: 5,
        responsive:{
            0:{
                items:1
            },
            481:{
                items:2
            },
            600:{
                items:2
            },
            768:{
                items:3
            },
            992:{
                items:4
            },
            1000:{
                items:5
            }
        }
    });


    /*=====================
     12. Service slider
     ==========================*/
    var ser_slider = $(".service_slider");
    ser_slider.owlCarousel({
        autoplay: true,
        loop: true,
        dots: false,
        margin: 30,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        items: 3,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:2
            },
            992:{
                items:3
            }
        }
    });


    /*=====================
     13. Project Slider
     ==========================*/
    var pro_slider = $(".project_slider");
    pro_slider.owlCarousel({
        autoplay: true,
        loop: true,
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        items: 4,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:3
            },
            992:{
                items:4
            }
        }
    });


    /*=====================
     14. portfolio slider
     ==========================*/
    var pro_slider = $(".portfolio_slider");
    pro_slider.owlCarousel({
        autoplay: true,
        loop: true,
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        items: 6,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1
            },
            576:{
                items:2
            },
            768:{
                items:3
            },
            992:{
                items:6
            }
        }
    });


    /*=====================
     15. Rating Start
     ==========================*/
    "use strict";
    $(document).ready(function(){
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });


    /*=====================
     16. Footer Title Start
     ==========================*/
    var contentwidth = jQuery(window).width();
    if ((contentwidth) < '768') {
        jQuery('.footer-title h3').append('<span class="according-menu"></span>');
        jQuery('.footer-title').on('click', function () {
            jQuery('.footer-title').removeClass('active');
            jQuery('.footer-contant').slideUp('normal');
            if (jQuery(this).next().is(':hidden') == true) {
                jQuery(this).addClass('active');
                jQuery(this).next().slideDown('normal');
            }
        });
        jQuery('.footer-contant').hide();
    } else {
        jQuery('.footer-contant').show();
    }


    /*=====================
     17. Search Start
     ==========================*/
    $('.search-button').on('click', function() {
        $(this).parent().toggleClass('open');
    });


    /*=====================
     18. Footer Fix
     ==========================*/
    var contentwidth = jQuery(window).width();
    if ((contentwidth) > '767') {
        $('footer').footerReveal();
    }


    /*=====================
     19. LTR to RTL JS
     ==========================*/

    $(".rtl-btn").click(function() {
        $(this).toggleClass("active");
        if ($(this).hasClass('active')) {
            $("html").attr("dir", "rtl");
            $("body").addClass('rtl');
            $('.RTL_cls').text('RTL');
        } else {
            $("html").attr("dir", "ltr");
            $("body").removeClass('rtl');
            $('.RTL_cls').text('LTR');
        }
    });


    /*=====================
     20. Color change
     ==========================*/
    (function() {
        $('<div class="color-picker">' +
            '<a href="#" class="handle">' +
            '<i class="fa fa-paint-brush" aria-hidden="true"></i>' +
            '</a><div class="sec-position"><div class="settings-header">' +
            '<h3>Select Color:</h3>' +
            '</div>' +
            '<div class="section">' +
            '<div class="colors o-auto">' +
            '<a href="#" class="color1" ></a>' +
            '<a href="#" class="color2" ></a>' +
            '<a href="#" class="color3" ></a>' +
            '<a href="#" class="color4" ></a>' +
            '<a href="#" class="color5" ></a>' +
            '<a href="#" class="color6" ></a>' +
            '<a href="#" class="color7" ></a>' +
            '<a href="#" class="color8" ></a>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>').appendTo($('body'));
    })();
    var body_event = $("body");
    body_event.on("click", ".color1", function() {
        $("#color" ).attr("href", "assets/css/color1.css" );
        return false;
    });
    body_event.on("click", ".color2", function() {
        $("#color" ).attr("href", "assets/css/color2.css" );
        return false;
    });
    body_event.on("click", ".color3", function() {
        $("#color" ).attr("href", "assets/css/color3.css" );
        return false;
    });
    body_event.on("click", ".color4", function() {
        $("#color" ).attr("href", "assets/css/color4.css" );
        return false;
    });
    body_event.on("click", ".color5", function() {
        $("#color" ).attr("href", "assets/css/color5.css" );
        return false;
    });
    body_event.on("click", ".color6", function() {
        $("#color" ).attr("href", "assets/css/color6.css" );
        return false;
    });
    body_event.on("click", ".color7", function() {
        $("#color" ).attr("href", "assets/css/color7.css" );
        return false;
    });
    body_event.on("click", ".color8", function() {
        $("#color" ).attr("href", "assets/css/color8.css" );
        return false;
    });
    $('.color-picker').animate({right: '-190px'});
    body_event.on("click", ".color-picker a.handle", function(e) {
        e.preventDefault();
        var div = $('.color-picker');
        if (div.css('right') === '-190px') {
            $('.color-picker').animate({right: '0px'});
        }
        else {
            $('.color-picker').animate({right: '-190px'});
        }
    });


})(jQuery);